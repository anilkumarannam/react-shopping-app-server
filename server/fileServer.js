const getProducts = require('./serverModules/getProducts');
const addItemToCart = require('./serverModules/addItemToCart');
const updateCartItemQuantity = require('./serverModules/updateCartItemQuantity');

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const removeItemFromCart = require('./serverModules/removeItemFromCart');
const getCart = require('./serverModules/getCart');
const placeOrder = require('./serverModules/placeOrder');
const getOrders = require('./serverModules/getOrders');
const getOrderDetails = require('./serverModules/getOrderDetails');
const getProductDetails = require('./serverModules/getProductDetails');

const server = express();
server.use(cors({ origin: true }));
server.use(bodyParser.json());       // to support JSON-encoded bodies
server.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
const port = 8000

server.get('/', async (req, res) => {
  const response = await getProducts();
  res.send(response);
});

server.post('/addToCart/', async (req, res) => {
  const { body } = req;
  const response = await addItemToCart(body);
  res.send(response);
});

server.delete('/removeFromCart/', async (req, res) => {
  const { body } = req;
  const response = await removeItemFromCart(body);
  res.send(response);
});

server.get('/cart/', async (req, res) => {
  const response = await getCart();
  res.send(response);
});

server.put('/updateCartItemQuantity/', async (req, res) => {
  const { body } = req;
  const response = await updateCartItemQuantity(body);
  res.send(response);
});

server.put('/placeOrder/', async (req, res) => {
  const response = await placeOrder();
  res.send(response);
});

server.get('/orders/', async (req, res) => {
  const response = await getOrders();
  res.send(response);
});


server.get('/orders/:id', async (req, res) => {
  const { id } = req.params;
  const response = await getOrderDetails(id);
  res.send(response);
});
server.get('/products/:id', async (req, res) => {
  const { id } = req.params;
  const response = await getProductDetails(id);
  res.send(response);
});


server.listen(port, () => {
  console.log(`Server running at ${port}`)
});
