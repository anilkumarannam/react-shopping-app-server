const readJsonData = require('../../modules/readJsonData');
const path = require('path');

const getCart = async () => {
  const filePath = path.resolve(__dirname, '../files/cart.json');
  const response = await readJsonData(filePath, 'utf-8');
  const keys = Object.keys(response);
  const cartItems = keys.map((key) => ({ id: key, ...response[key] }));
  return cartItems;
}

module.exports = getCart;