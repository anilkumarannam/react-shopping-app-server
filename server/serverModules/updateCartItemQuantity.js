const readJsonData = require('../../modules/readJsonData');
const writeJsonData = require('../../modules/writeJsonData');
const path = require('path');

const updateCartItemQuantity = async (body) => {

  const { itemId, quantity } = body
  const filePath = path.resolve(__dirname, '../files/cart.json');
  const cartData = await readJsonData(filePath, 'utf-8');
  cartData[itemId]['quantity'] = quantity;
  const keys = Object.keys(cartData);
  const cartItems = keys.map((key) => ({ id: key, ...cartData[key] }));
  await writeJsonData(filePath, cartData);
  return cartItems;

}

module.exports = updateCartItemQuantity;