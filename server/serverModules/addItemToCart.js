const readJsonData = require('../../modules/readJsonData');
const writeJsonData = require('../../modules/writeJsonData');
const path = require('path');
const getProducts = require('./getProducts');

const addItemToCart = async (body) => {

  const { itemId } = body
  const filePath = path.resolve(__dirname, '../files/cart.json');
  const { products } = await getProducts();
  const cartItem = products.find((product) => itemId === product.id);
  const { id, ...cart } = cartItem;
  const cartData = await readJsonData(filePath, 'utf-8');
  cartData[id] = { ...cart, quantity: 1 };
  await writeJsonData(filePath, cartData);
  return cartData;

}

module.exports = addItemToCart;