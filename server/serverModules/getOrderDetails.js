const readJsonData = require('../../modules/readJsonData');
const path = require('path');

const getOrderDetails = async (id) => {
  const filePath = path.resolve(__dirname, '../files/orders.json');
  const response = await readJsonData(filePath, 'utf-8');
  const order = response[id];
  return order;
}

module.exports = getOrderDetails;