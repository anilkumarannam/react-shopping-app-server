const readJsonData = require('../../modules/readJsonData');
const path = require('path');

const getOrders = async () => {
  const filePath = path.resolve(__dirname, '../files/orders.json');
  const response = await readJsonData(filePath, 'utf-8');
  const keys = Object.keys(response);
  const cartItems = keys.map((key) => ({ id: key, ...response[key] }));
  return cartItems;
}

module.exports = getOrders;