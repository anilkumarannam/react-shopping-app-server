const readJsonData = require('../../modules/readJsonData');
const path = require('path');

const getProducts = async () => {
  const filePath = path.resolve(__dirname, '../files/products.json');
  const response = await readJsonData(filePath, 'utf-8');
  return response;
}

module.exports = getProducts;