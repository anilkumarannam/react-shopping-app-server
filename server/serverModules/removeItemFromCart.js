const readJsonData = require('../../modules/readJsonData');
const writeJsonData = require('../../modules/writeJsonData');
const path = require('path');
const getProducts = require('./getProducts');

const removeItemFromCart = async (body) => {

  const { itemId } = body
  const filePath = path.resolve(__dirname, '../files/cart.json');
  const cartData = await readJsonData(filePath, 'utf-8');
  delete cartData[itemId];
  await writeJsonData(filePath, cartData);
  return cartData;

}

module.exports = removeItemFromCart;