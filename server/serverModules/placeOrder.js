const processCartData = require('../../modules/processCartData');
const generateOrderId = require('../../modules/generateOrderId');
const writeJsonData = require('../../modules/writeJsonData');
const readJsonData = require('../../modules/readJsonData');
const getCart = require('./getCart');
const path = require('path');

const placeOrder = async () => {
  const filePath = path.resolve(__dirname, '../files/orders.json');
  const orders = await readJsonData(filePath);
  const cartItems = await getCart();
  const cartInfo = processCartData(cartItems);
  const { subTotal } = cartInfo;
  const orderId = generateOrderId();
  const updatedOrders = { ...orders, [orderId]: { cartItems, subTotal } };
  const cartPath = path.resolve(__dirname, '../files/cart.json');
  await writeJsonData(filePath, updatedOrders);
  await writeJsonData(cartPath, {});
  return updatedOrders;
}

module.exports = placeOrder;