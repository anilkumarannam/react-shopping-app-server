const readJsonData = require('../../modules/readJsonData');
const path = require('path');

const getProductDetails = async (id) => {
  const filePath = path.resolve(__dirname, '../files/products.json');
  const response = await readJsonData(filePath, 'utf-8');
  const { products } = response;
  const product = products.find((item) => String(item.id) === id);
  return product;
}

module.exports = getProductDetails;