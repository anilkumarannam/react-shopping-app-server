const transformUrl = (inputUrl) => {
  return "https://drive.google.com/uc?id=".concat(inputUrl.split('/')[5]);
}

export default transformUrl;