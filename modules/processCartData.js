const processCartData = (cart) => {
  const cartInfo = { totalItems: cart.length };
  const subTotal = cart.reduce((total, cartItem) => total + cartItem.price * cartItem.quantity, 0);
  return { cartItems: cart, ...cartInfo, subTotal };
}

module.exports = processCartData;