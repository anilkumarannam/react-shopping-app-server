const fs = require('fs');
const path = require('path');
const writeJsonData = require('./writeJsonData');
const getCart = require('./getCart');

const updateCart = async (body) => {
  const cart = await getCart();
  await writeJsonData("../../public/cart.json", cart);
}

module.exports = updateCart;