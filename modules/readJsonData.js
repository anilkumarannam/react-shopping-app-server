const { readFile } = require('fs/promises');

const readJsonData = async (filePath) => {
  const readData = await readFile(filePath);
  const jsonResponse = JSON.parse(readData);
  return jsonResponse;
}

module.exports = readJsonData;