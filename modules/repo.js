const repo = () => {
  const repository = {
    products: [],
    cart: {},
    orders: {}
  }


  const initializeProducts = (productValues) => {

    repository.products = productValues;
    repository.cart = {};

  }
  const initializeCart = (cartItems) => {
    repository.cart = cartItems;
  }
  const initializeOrders = (userOrders) => {
    repository.orders = userOrders;
  }

  const addItemToCart = (itemId, quantity) => {
    if (!(itemId in repository.cart)) {
      repository.cart[itemId] = quantity;
    }
    return JSON.stringify(repository);
  }

  const getUpdatedCart = () => {
    const { products } = repository.products;
    const { cart } = repository;
    if (products) {
      const updatedCartItems = products.filter((product) => String(product.id) in cart);
      return JSON.stringify({ updatedCartItems });
    }
    return JSON.stringify({});
  }

  const deleteItemFromCart = (itemId) => {
    if (itemId in repository.cart) {
      delete repository.cart[itemId];
    }
    return JSON.stringify(repository);
  }

  const getCartItems = () => {
    console.log(repository.cart);
  }

  return {
    initializeProducts,
    initializeCart,
    initializeOrders,
    addItemToCart,
    deleteItemFromCart,
    getUpdatedCart,
    getCartItems
  };

}

module.exports = repo;