const { writeFile } = require('fs/promises');
const path = require('path');

const writeJsonData = async (filePath, dataObject) => {
  await writeFile(filePath, JSON.stringify(dataObject));
}

module.exports = writeJsonData;